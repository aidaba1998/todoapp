import React from 'react'
import {db} from './firebase';
import{collection,addDoc} from 'firebase/firestore';
import {useState} from 'react'

function AddTodo() {
    const [title,setTitle] = useState('');
    const handleSubmit= async(e)=>{
        e.preventDefault();
        if(title!==""){
            await addDoc(collection(db,"todos"),{
               title,
               completed:false, 
            }); 
            setTitle("");
        }
    }
    return (
        <form onSubmit={handleSubmit}>
        <div className='input_container  '>
        <input type='text' className='input' placeholder='New todo' onChange={(e)=>setTitle(e.target.value)}
         value={title} />

        </div>
        <div className='btn_container'>
        <button>Add</button>

        </div>

        </form>
    )
}

export default AddTodo
