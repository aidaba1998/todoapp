
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyB4Z1uBB5L-hjLxJtqRVDUTU2g-voVqtmI",
  authDomain: "todoapp-7ec9f.firebaseapp.com",
  projectId: "todoapp-7ec9f",
  storageBucket: "todoapp-7ec9f.appspot.com",
  messagingSenderId: "304295724690",
  appId: "1:304295724690:web:cc0896c06c2b39e3fc084b",
  measurementId: "G-LYK4FB716C"
};

  
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);